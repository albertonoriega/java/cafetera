
package cafeteraapp;


public class Cafetera {
    public int capacidadMaxima;
    public int cantidadActual;

    public Cafetera() {
        this.capacidadMaxima = 100;
        this.cantidadActual = 0;
    }

    public Cafetera(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
        this.cantidadActual = 0;
    }

    public Cafetera(int capacidadMaxima, int cantidadActual) {
        this.capacidadMaxima = capacidadMaxima;
        if (cantidadActual > capacidadMaxima){
            this.cantidadActual= capacidadMaxima;
        }else {
            this.cantidadActual = cantidadActual;    
        }
        
    }

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }

    public int getCantidadActual() {
        return cantidadActual;
    }

    public void setCantidadActual(int cantidadActual) {
        this.cantidadActual = cantidadActual;
    }
    
    public void llenarCafetera (){
        setCantidadActual(this.capacidadMaxima);
    }
    
    public void servirTaza (int cantidad){
        if (this.cantidadActual< cantidad){
            setCantidadActual(0);
        } else {
        setCantidadActual(this.cantidadActual-cantidad);
        }
    }
    
    public void vaciarCafetera() {
        setCantidadActual(0);
    }
    
    public String agregarCafe (int cantidad) {
        if(this.cantidadActual+cantidad > this.capacidadMaxima){
            setCantidadActual(this.capacidadMaxima);
            return "Has llenado de mas la cafetera";
        }else {
            setCantidadActual(this.cantidadActual+cantidad);
            return "";
        }
    }
    
    public int ocupacion (){
        int ocupacion = 100 *this.cantidadActual / this.capacidadMaxima;
        return ocupacion;
    }

    @Override
    public String toString() {
        return "La cafetera tiene una capacidad máxima de: " + capacidadMaxima + " y su cantidad actual es: " + cantidadActual;
    }
    
    
}
