
package cafeteraapp;


public class CafeteraApp {

    
    public static void main(String[] args) {
        
        
        Cafetera bialetti, moka, taurus;
        
        bialetti = new Cafetera();
        moka = new Cafetera(2000);
        taurus = new Cafetera(2500, 750);
        
        // Cambia la capacidad maxima de bialetti a 3000cc
        System.out.println("Capacidad maxima bialetti: " + bialetti.getCapacidadMaxima());
        bialetti.setCapacidadMaxima(3000);
        System.out.println("Capacidad maxima bialetti: " + bialetti.getCapacidadMaxima());
        
        // Vacia la cafetera moka
        moka.vaciarCafetera();
        
        // Llena la cafetera taurus
        taurus.llenarCafetera();
        
        // Sirve cafe de 100 cc con la cafetera moka
        
        moka.servirTaza(100);
        
        // Añade cafe a la cafetera moka (1500 cc)
        
        System.out.println(moka.agregarCafe(1500));
        
        // Añade ahora 800 cc a la cafetera moka
        
        System.out.println(moka.agregarCafe(800));
        
        System.out.println(taurus);
        System.out.println(bialetti);
        System.out.println(moka);
        
        
    }
    
}
